/*#############################################################################
 *###
 *###   Demo for CE related to PWM
 *###   ===========
 *#############################################################################
 *
 * Code belongs to TI4 CE; Informatik; HAW-Hamburg; Berliner Tor 7; D-20099 Hamburg
 * Code is based on demo examples from Silke Behn, Heiner Heitmann, Heiko Rowedder & Bernd Schwarz
 *
 *-----------------------------------------------------------------------------
 * Description:
 * ============
 * Code is an example for simple PWM application.
 * PWM1.1 is fixed to 50% duty cycle
 * PWM1.2 is running cyclic sequence:   0/8, 1/8, 2/8, 3/8, 4/8, 5/8, 6/8, 7/8, 8/8,  0/8, 1/8, ...
 * Led1 (PORTA.0) is used as trigger for new PWM1.2 signal cycle
 *
 *-----------------------------------------------------------------------------
 * Abbreviations:
 * ==============
 *
 * RM ::= Reference Manual RM0090 ; DM00031020 ;  DocID 018909 Rev9 ;  2015-03
 *
 *-----------------------------------------------------------------------------
 * History:
 * ========
 *   150518: code ported to Silica Xynergy Board by Yannic Wilkening & Florian Meyer
 *   For "full history" see ReadMe.txt
 *-----------------------------------------------------------------------------
 */

#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stdint.h>
#include "CE_Lib.h"
#include "tft.h"


// current version of sample code
#define VERSION "v1.00" 



// comment line if you dont want to use LCD info
#define USE_LCD 



// macro converts binary value (containing up to 8 bits resp. <=0xFF) to unsigned decimal value
// as substitute for missing 0b prefix for binary coded numeric literals.
// macro does NOT check for an invalid argument at all.
#define b(n) (                                               \
    (unsigned char)(                                         \
            ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
        |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
        |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                                        \
)


// simplified acces to switches S0 - S7
#define  S1   ( !(GPIOH->IDR & (1 << 15)) )
#define  S2   ( !(GPIOH->IDR & (1 << 12)) )
#define  S3   ( !(GPIOH->IDR & (1 << 10)) )
#define  S4   ( !(GPIOF->IDR & (1 << 8 )) )
#define  S5   ( !(GPIOF->IDR & (1 << 7 )) )
#define  S6   ( !(GPIOF->IDR & (1 << 6 )) )
#define  S7   ( !(GPIOC->IDR & (1 << 2 )) )
#define  S8   ( !(GPIOI->IDR & (1 << 9 )) )


// Timer setup
// ============
#define SYS_FREQ    168000000                                       // 168MHz system frequency
#define TIMER_FREQ      48000                                       // 48KHz requested timer frequency


//----------------------------------------------------------------------------
//
//  ISR
//
void TIM8_UP_TIM13_IRQHandler(void) {                               // Timer interrupt fired with 48000Hz
    static uint8_t isrCnt = 0;
	TIM8->SR = ~TIM_SR_UIF;                                         // Reset interrupt flag to receive new interrupts (RM0090 Chap 17.4.5) 
    
    // handle IRQ  resp. do service
    // ==========
    //
		
	(!isrCnt) ? setLED(1) : resetLED(1);							// just a trigger for oscilloscope (PORTA.0) to mark new cycles
	TIM8->CCR3 = (((SYS_FREQ / TIMER_FREQ) / 8) -1) * isrCnt++;		// counts in 8 steps from 0% to 100% duty cycle (change compare value)
  	isrCnt %= 8;

}//TIM8_UP_TIM13_IRQHandler()


//----------------------------------------------------------------------------
//
//  MAIN
//
int main( void ){

   
    // general setup
    // =============
    //
    initCEP_Board();                                // initilize display, leds, buttons, uart and other stuff
        
    
    // clock setup (enable clock for used ports)
    // =============
    //
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;            // enable clock for GPIOA (RM0090 Chap 6.3.10)
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;            // enable clock for GPIOB
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;            // enable clock for GPIOC
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;            // enable clock for GPIOF
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;            // enable clock for GPIOH
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;            // enable clock for GPIOI
	
	resetLED(1); // set trigger pin to low
	
    // set IO mode to alternate function (RM0090 Chap 8.3.2)
	GPIOB->MODER   |= (GPIO_Mode_AF 	<< (GPIO_PinSource0 * 2))	| (GPIO_Mode_AF     << (GPIO_PinSource1 * 2));
	GPIOB->OSPEEDR |= (GPIO_Speed_50MHz << (GPIO_PinSource0 * 2)) 	| (GPIO_Speed_50MHz << (GPIO_PinSource1 * 2));
	GPIOB->OTYPER  |= (GPIO_OType_PP 	<< (GPIO_PinSource0))       | (GPIO_OType_PP    << (GPIO_PinSource1));
	GPIOB->PUPDR   |= (GPIO_PuPd_UP 	<< (GPIO_PinSource0 * 2))   | (GPIO_PuPd_UP     << (GPIO_PinSource1 * 2));
    // set alternate function mode for use as pwm output (RM0090 Chap 8.3.2)
	GPIOB->AFR[0]  |= (GPIO_AF_TIM8 	<< (GPIO_PinSource0 * 4))   | (GPIO_AF_TIM8     << (GPIO_PinSource1 * 4));
        
    
    // Timer
    // =====
    //

    RCC->APB2ENR |= RCC_APB2ENR_TIM8EN;             // enable clock for timer 8         (RM0090 Chap 6.3.14)
    TIM8->CR1 = 0;                                  // disabled timer                   (RM0090 Chap 17.4.1)
    TIM8->CR2 = 0;                                  //                                  (RM0090 Chap 17.4.2)
    TIM8->PSC = 0;                                  // prescaler                        (RM0090 Chap 17.4.11)
    TIM8->ARR = (SYS_FREQ / TIMER_FREQ) -1;         // auto reload register for 48000Hz (RM0090 Chap 17.4.12)
    TIM8->DIER = TIM_DIER_UIE;                      // enable interrupt                 (RM0090 Chap 17.4.4)
    TIM8->CR1 = TIM_CR1_ARPE;                       // enable preload                   (RM0090 Chap 17.4.1)
	TIM8->BDTR = TIM_BDTR_MOE;
    
    // set output compare modes for corresponding IO pins (RM0090 Chap 17.4.7 / 17.4.8) 
	TIM8->CCMR1 = TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE;
	TIM8->CCMR2 = TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3PE;
    // set complementary output enable for corresponding IO pins (RM0090 Chap 17.4.9)
	TIM8->CCER = TIM_CCER_CC3NE | TIM_CCER_CC2NE;
    
    NVIC_SetPriorityGrouping(2);                    // set priority
    NVIC_SetPriority(TIM8_UP_TIM13_IRQn, 0);        // set IRQ handler
    NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);             // enable IRQ 
    
    // LCD info
    // ========
    //
#ifdef USE_LCD
    TFT_cls();
    TFT_gotoxy(1,1);
    TFT_puts("[Demo for CE related to PWM]");
    TFT_gotoxy(1,2);
    TFT_puts(VERSION);
#endif
    
	// PWM Output1 Default values for compare register 50% duty cycle
    TIM8->CCR2 = ((SYS_FREQ / TIMER_FREQ) / 2) -1; 	                    // (RM0090 Chap 18.3.8)
	// PWM Output 2
    TIM8->CCR3 = 0;
    
	// start action by starting timer
    TIM8->CR1 |= TIM_CR1_CEN;                                           // (RM0090 Chap 17.3.4)

    // NEVER stop doing task
    while(1){
		//
    }//while

}//main()
